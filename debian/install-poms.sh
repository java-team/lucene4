#!/bin/sh

set -e

VERSION=$1
MAJOR=$(echo $VERSION | sed 's/\..*/.x/')
LOCAL_REPO=$(pwd)/debian/tmp/usr/share/maven-repo

rm -rf $LOCAL_REPO
/usr/share/maven-debian-helper/copy-repo.sh $(dirname $LOCAL_REPO)
# Make sure we don't pull in any previous Lucene versions
rm -rf $LOCAL_REPO/org/apache/lucene

mkdir -p debian/tmp/usr/share/java

find build -name 'lucene-*.jar' |
tee -a /dev/stderr |
while read file; do
    basename=$(basename $file)
    artifact=$(basename $file -$VERSION.jar)

    install -m644 $file debian/tmp/usr/share/java/

    for version in $VERSION $MAJOR; do
        mkdir -p debian/tmp/usr/share/maven-repo/org/apache/lucene/$artifact/$version
        echo "\
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<project xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\" xmlns=\"http://maven.apache.org/POM/4.0.0\"
    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
  <modelVersion>4.0.0</modelVersion>
  <groupId>org.apache.lucene</groupId>
  <artifactId>$artifact</artifactId>
  <version>$version</version>
  <description>dummy POM</description>
  <packaging>jar</packaging>
</project>\
" > debian/tmp/usr/share/maven-repo/org/apache/lucene/$artifact/$version/$artifact-$version.pom
        ln -rs debian/tmp/usr/share/java/$basename \
            debian/tmp/usr/share/maven-repo/org/apache/lucene/$artifact/$version/$artifact-$version.jar
    done
done
